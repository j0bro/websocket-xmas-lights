/*
 * NodeJS WebSocket X-Mas lights control server.
 *
 * Copyright (c) 2016 Jeroen Brosens
 */
'use strict';

var express = require('express');
var SocketServer = require('ws').Server;
var path = require('path');
var five = require("johnny-five");
var pixel = require("node-pixel");

const LED_PIN = 5;
const NUM_LEDS = 16;
const PORT = process.env.PORT || 3000;
const INDEX = path.join(__dirname, 'index.html');

var leds;

// Setup HTTP server
var server = express()
  .use((req, res) => res.sendFile(INDEX))
  .listen(PORT, '0.0.0.0', () => console.log('Listening on ' + PORT));

// Setup Websocket server
var numClients = 0;
var wss = new SocketServer({ server });

// Accept connections
wss.on('connection', (ws) => {
  if (++numClients > NUM_LEDS) {
    ws.send(JSON.stringify({ error: "Too many clients connected." }));
    ws.close();
  }
  ws.index = numClients;

  console.log('Client #' + ws.index + ' connected');

  ws.on('close', () => console.log('Client #' + ws.index + ' disconnected.'));

  ws.on('message', (message) => {
    if (ws.index > NUM_LEDS) {
      return;
    }
    var brightness = parseInt(message);

    // Sanitize
    if (brightness < 0) brightness = 0;
    if (brightness > 255) brightness = 255;

    leds[ws.index - 1] = brightness;
  });
});

// Broadcast to all clients every second
setInterval(() => {
  var time = new Date().toLocaleTimeString();
  wss.clients.forEach((client) => {
    var payload = {
      client: client.index,
      time: time
    };

    client.send(JSON.stringify(payload)); // Sending JSON object as String
  });
}, 1000);

// Setup Arduino
var board = new five.Board();
board.on("ready", function() {

  // Setup LED strip
  var strip = new pixel.Strip({
    board: this,
    controller: "FIRMATA",
    strips: [{
      pin: LED_PIN,
      length: NUM_LEDS
    }]
  });

  strip.on("ready", function() {
    leds = new Array(NUM_LEDS).fill(0.1 * 255 | 0); // initialize all leds at 10% brightness

    rainbow(strip, 20);
  });
});

function rainbow(strip, delay) {
  var color;
  var colorWheelIndex = 0;

  setInterval(function () {
    if (++colorWheelIndex > 255) {
      colorWheelIndex = 0;
    }

    for (var i = 0; i < NUM_LEDS; i++) {
      color = rgb((colorWheelIndex + i) & 255, leds[i]);
      strip.pixel(i).color(color);
    }
    strip.show();
  }, 1000 / delay);
}

function rgb(colorIndex, brightness) {
  var r, g, b;
  colorIndex = 255 - colorIndex;
  brightness = brightness / 255;

  if (colorIndex < 85) {
    r = 255 - colorIndex * 3;
    g = 0;
    b = colorIndex * 3;
  } else if (colorIndex < 170) {
    colorIndex -= 85;
    r = 0;
    g = colorIndex * 3;
    b = 255 - colorIndex * 3;
  } else {
    colorIndex -= 170;
    r = colorIndex * 3;
    g = 255 - colorIndex * 3;
    b = 0;
  }

  // Apply brightness
  r = r * brightness | 0;
  g = g * brightness | 0;
  b = b * brightness | 0;

  return "rgb(" + r + "," + g + "," + b + ")";
}
