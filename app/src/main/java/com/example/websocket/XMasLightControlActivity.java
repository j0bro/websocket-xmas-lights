/*
 * Android WebSocket X-Mas lights control client.
 *
 * Copyright (c) 2016 Jeroen Brosens
 */
package com.example.websocket;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class XMasLightControlActivity extends AppCompatActivity {

    private boolean isConnected;
    private Button btnConnect;
    private EditText editUri;
    private TextView txtMessages;
    private SeekBar slider;

    private WebSocketClient webSocketClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editUri = (EditText) findViewById(R.id.editUri);
        txtMessages = (TextView) findViewById(R.id.txtMessages);

        // Connect button
        btnConnect = (Button) findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected) {
                    closeWebSocket();
                } else {
                    connectWebSocket(String.valueOf(editUri.getText()));
                }
            }
        });

        // Slider
        slider = (SeekBar) findViewById(R.id.slider);
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (webSocketClient == null) {
                    return;
                }
                webSocketClient.send(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Unused
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Unused
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateView(isConnected);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (webSocketClient == null) {
            return;
        }
        webSocketClient.close();
    }

    private void connectWebSocket(String origin) {
        // Obtain URI
        URI uri;
        try {
            uri = new URI(origin);
        } catch (URISyntaxException e) {
            logMessage("Invalid URI.");

            return;
        }

        // Setup Websocket for URI
        webSocketClient = new WebSocketClient(uri) {

            @Override
            public void onOpen(ServerHandshake handshakedata) {
                updateView(true);
                logMessage("Connection opened.");
            }

            @Override
            public void onMessage(String message) {
                logMessage("Got message: " + message);
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                updateView(false);
                logMessage("Connection closed.");
            }

            @Override
            public void onError(Exception ex) {
                logMessage("ERROR: " + ex.getMessage());
            }
        };

        // Connect the Websocket
        webSocketClient.connect();
    }

    private void closeWebSocket() {
        if (webSocketClient == null) {
            return;
        }
        webSocketClient.close();
    }

    private void updateView(final boolean isConnected) {
        this.isConnected = isConnected;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                editUri.setEnabled(!isConnected);
                btnConnect.setText(isConnected ? R.string.lblClose : R.string.lblConnect);
                slider.setEnabled(isConnected);

                editUri.requestFocus();
            }
        });
    }

    private void logMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtMessages.append(message);
                txtMessages.append("\n");
            }
        });
    }
}
