# WebSocket X-Mas lights

## Prerequisites

1. Install NodeJS
2. Install Android Studio

## Getting Started

1. Clone repository
2. Open a terminal 
3. cd to /js folder
4. Run 'npm install' to install dependencies
5. Connect Arduino flashed with 'StandardFirmata' sketch
6. Run 'npm start'
7. Compile and run Arduino app from Android Studio
8. Connect client(s) to <IP-address>:3000